package com.calcard.config.constants;


public final class SecurityConstants {

    public static final String AUTH_LOGIN_URL = "/oauth/authenticate";

    public static final String JWT_SECRET = "y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRfUjXn2r5u8x/A?D(G+KbPdSgVkYp3s";

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "calcard-api";
    public static final String TOKEN_AUDIENCE = "calcard-app";
    public static final long   TOKEN_EXPIRATION = 860_000_000;

    private SecurityConstants() {}
}
