package com.calcard.config.constants;

public final class RulesConstants {
	
    public static final double MIN_APPROVAL = 300;
    public static final double SINGLE_REDUCTION_RATE = 0.4;
    public static final double MARRIED_REDUCTION_RATE = 0.8;
    public static final double WIDOWED_REDUCTION_RATE = 0.5;
    public static final double DIVORCED_REDUCTION_RATE = 0.36;
    public static final int MAX_DEPENDENTS_WIDOWED = 1;
    public static final int MAX_DEPENDENTS_DIVORCED = 0;
    public static final int MAX_DEPENDENTS_SINGLE = 2;
   

    private RulesConstants() {}

}
