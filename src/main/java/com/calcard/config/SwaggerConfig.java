package com.calcard.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.calcard.config.constants.SecurityConstants;
import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    public static final String DEFAULT_INCLUDE_PATTERN = "/v1/api/.*";
    @Bean
    public Docket swaggerSpringfoxDocket() {
 

        Docket docket = new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(this.APIInfo().build())
            .forCodeGeneration(true)
            .securityContexts(Lists.newArrayList(securityContext()))
            .securitySchemes(Lists.newArrayList(apiKey()))
            .useDefaultResponseMessages(false);

        docket = docket.select()
            .paths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
            .build();
        return docket;
    }


    private ApiKey apiKey() {
        return new ApiKey(SecurityConstants.TOKEN_TYPE, SecurityConstants.TOKEN_HEADER, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
            .securityReferences(defaultAuth())
            .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
            .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
            = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
            new SecurityReference("JWT", authorizationScopes));
    }
   
	
	private ApiInfoBuilder APIInfo() {

		ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();

		apiInfoBuilder.title("API-Proposta Calcard");
		apiInfoBuilder.description("API para inserção e consulta de propostas");
		apiInfoBuilder.version("1.0");
		apiInfoBuilder
				.termsOfServiceUrl("Termo de uso: Somente para fins de comprovação de conhecimento na tecnologia.");
		apiInfoBuilder.license("Licença - Open Source");
		apiInfoBuilder.licenseUrl("https://bitbucket.org/W1LLMORA3S/willian-calcard-tech-evaluation/src/master/");
		apiInfoBuilder.contact(this.contact());

		return apiInfoBuilder;

	}

	private Contact contact() {

		return new Contact("Willian Moraes",
				"https://bitbucket.org/W1LLMORA3S/willian-calcard-tech-evaluation/src/master/",
				"willian_200@hotmail.com.br");
	}
}