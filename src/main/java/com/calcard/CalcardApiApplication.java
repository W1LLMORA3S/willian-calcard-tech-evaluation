package com.calcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalcardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalcardApiApplication.class, args);
	}

}
