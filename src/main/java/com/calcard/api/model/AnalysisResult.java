package com.calcard.api.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="Resultado da Análise", description="Objeto contedo o resultado da análise no motor de regras")
public class AnalysisResult {

	@ApiModelProperty(hidden = true)
	private int id;
	@ApiModelProperty(value = "Código da proposta na qual o resultado foi obtido" )
	private int idProposal;
	@ApiModelProperty(value = "Limite mínimo de crédito aprovado",allowEmptyValue = true )
	private double limitMin;
	@ApiModelProperty(value = "Limite máximo de crédito aprovado",allowEmptyValue = true )
	private double limitMax;
	@ApiModelProperty(value = "Status do processamento da proposta enviada",allowableValues = "APROVADO, REPROVADO, ERRO")
	private String statusProposal;
	@ApiModelProperty(value = "Exibe mensagens com informações importantes. Em caso de erro mostra a mensagem do que ocasionou.")
	private String message;
	@ApiModelProperty(value = "Data em que o processo foi executado")
	private Date createdDate;
}
