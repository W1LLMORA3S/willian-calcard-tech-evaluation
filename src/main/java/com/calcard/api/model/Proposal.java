package com.calcard.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Proposal {
	@ApiModelProperty(hidden = true)
	private int id;
	@ApiModelProperty(value = "CPF completo do cliente",example = "XXXXXXXXXXX" )
	private String cpf;
	@ApiModelProperty(value = "Nome completo do cliente")
	private String name;
	@ApiModelProperty(value = "Idade do cliente")
	private int age;
	@ApiModelProperty(value = "Sexo do cliente",allowableValues = "M,F")
	private char sex;
	@ApiModelProperty(value = "Estado civil do cliente",allowableValues = "SOLTEIRO,CASADO, DIVORCIADO, VIÚVO")
	private String maritalStatus;
	@ApiModelProperty(value = "Sigla do estado onde reside")
	private String state;
	@ApiModelProperty(value = "Quantidade de dependentes")
	private int dependents;
	@ApiModelProperty(value = "Renda mensal",example = "500.50")
	private double income;
	@ApiModelProperty(hidden = true)
	private double incomeReal;
	@ApiModelProperty(hidden = true)
	private boolean approve;
}
