package com.calcard.api.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Messages {

	LOW_INCOME("Renda baixa"), CREDIT_POLICY("Reprovado pela política de crédito");

	@Getter
	private String disapprovedMessage;
}
