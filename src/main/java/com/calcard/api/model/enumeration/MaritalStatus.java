package com.calcard.api.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum MaritalStatus {

	SINGLE("SOLTEIRO"), MARRIED("CASADO"), DIVORCED("DIVORCIADO"), WIDOWED("VIÚVO");

	@Getter
	private String maritalStatus;
}
