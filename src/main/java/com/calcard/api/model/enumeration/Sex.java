package com.calcard.api.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Sex {

	MALE("M"), FEMALE("F");

	@Getter
	private String sexChar;
}
