package com.calcard.api.model.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum StatusProposal {

	APPROVED("APROVADO"), DISAPPROVED("REPROVADO"), ERROR("ERRO");

	@Getter
	private String statusProposal;
}
