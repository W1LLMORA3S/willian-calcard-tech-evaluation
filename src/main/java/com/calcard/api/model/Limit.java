package com.calcard.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Limit {
	private int id;
	@ApiModelProperty(value = "Valor do limite mínimo de crédito")
	private double min;
	@ApiModelProperty(value = "Valor do limite máximo de crédito")
	private double max;
	@ApiModelProperty(value = "Descrição do limite de crédito")
	private String description;
}
