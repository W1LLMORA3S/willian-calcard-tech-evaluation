package com.calcard.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.calcard.api.model.AnalysisResult;
import com.calcard.api.repository.AnalysisResultRepository;
import com.calcard.api.service.AnalysisResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("v1/api")
@Service
@Api(tags = "RESULTADOS DA ANÁLISE", description = "API para análise busca e visualização dos resultados da análise")
public class AnalysisResultServiceImpl implements AnalysisResultService {
	
	@Autowired
	private AnalysisResultRepository analysisResultRepository;

	@ApiOperation(value = "Busca um resultado da análise pelo cpf", response = AnalysisResult.class, notes = "Esse metódo recebe o cpf de beneficiário e retorna o resultado da análise")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto analysisResult completo com o código de cpf informado.", response = AnalysisResult.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/analysisResultByCpf/{cpf}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public AnalysisResult findByCPF(String cpf) {
		return analysisResultRepository.findResultByCpf(cpf);
	}

	@ApiOperation(value = "Busca um resultado da análise pelo id", response = AnalysisResult.class, notes = "Esse metódo recebe o id de um resultado de análise e retorna os resultados")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto analysisResult completo com o código de id informado.", response = AnalysisResult.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/analysisResult/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public AnalysisResult findByIdProposal(@PathVariable("id") long id) {
		return analysisResultRepository.findResultByIdProposal(id);
	}

	@ApiOperation(value = "Busca todos os resultados de análise da base", response = AnalysisResult.class, notes = "Esse metódo retorna todos os resultados de análise cadastrados na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um lista de objetos analysisResult completos ", response = AnalysisResult.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/analysisResult", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public List<AnalysisResult> findAll() {
		return analysisResultRepository.findAll();
	}

}
