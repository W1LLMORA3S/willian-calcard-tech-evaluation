package com.calcard.api.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.Response;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.api.repository.AnalysisResultRepository;
import com.calcard.api.repository.ProposalRepository;
import com.calcard.api.service.EngineService;
import com.calcard.api.service.ProposalService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("v1/api")
@Service
@Api(tags = "PROPOSTA", description = "API para cadastro e busca de propostas no sistema")
public class ProposalServiceImpl implements ProposalService {

	@Autowired
	private ProposalRepository proposalRepository;

	@Autowired
	private AnalysisResultRepository analysisResultRepository;

	@Autowired
	private EngineService engineService;

	@ApiOperation(value = "Cadastrar uma nova proposta", response = AnalysisResult.class, notes = "Esse metódo recebe os dados da proposta e salva na base de dados")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna a análise do resultado, indicando a aprovação, reprovação ou erro no processamento da proposta.", response = AnalysisResult.class),
			@ApiResponse(code = 500, message = "Em caso de erro, retorna o objeto resultado da análise com o status ERRO e a mensagem no campo message", response = AnalysisResult.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposal", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody AnalysisResult insert(@RequestBody Proposal proposal) {
		try {

			Proposal proposalExists = proposalRepository.findByCpf(proposal.getCpf());

			if (proposalExists == null) {

				int idProposal = proposalRepository.insert(proposal);
				AnalysisResult analysisResult = engineService.analyzeProposal(proposal);

				analysisResult.setCreatedDate(new Date());
				analysisResult.setIdProposal(idProposal);
				proposal.setId(idProposal);

				analysisResultRepository.insert(analysisResult);
				proposalRepository.update(proposal);

				return analysisResult;

			} else {
				throw new java.lang.Error("Já existe proposta cadastrada para este CPF na base, tente buscar pelo ID: "
						+ proposalExists.getId());
			}

		} catch (Exception e) {

			AnalysisResult result = new AnalysisResult();
			result.setStatusProposal(StatusProposal.ERROR.getStatusProposal());
			result.setMessage(e.getMessage());

			return result;
		}
	}

	@ApiOperation(value = "Atualizar uma proposta", response = Response.class, notes = "Esse metódo recebe os dados de uma proposta e atualiza os dados da mesma na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto response com código 1 caso a atualização tenha sido feita com sucesso.", response = Response.class),
			@ApiResponse(code = 500, message = "Retorna um objeto response com código 0 e o erro ocassionado.", response = Response.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposal", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Response update(@RequestBody Proposal proposal) {
		try {

			proposalRepository.update(proposal);

			return new Response(1, "Proposta atualizada com sucesso!");

		} catch (Exception e) {

			return new Response(0, e.getMessage());
		}
	}

	@ApiOperation(value = "Deletar uma proposta", response = Response.class, notes = "Esse metódo recebe o id de uma proposta e deleta a mesma na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto response com código 1 caso o delete tenha sido feita com sucesso.", response = Response.class),
			@ApiResponse(code = 500, message = "Retorna um objeto response com código 0 e o erro ocassionado.", response = Response.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposal/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Response delete(@PathVariable("id") long id) {
		try {

			proposalRepository.delete(id);

			return new Response(1, "Proposta deletada com sucesso!");

		} catch (Exception e) {

			return new Response(0, e.getMessage());
		}
	}

	@ApiOperation(value = "Busca uma proposta pelo id", response = Proposal.class, notes = "Esse metódo recebe o id de uma proposta e retorna a proposta completa")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto proposal completo com o código de id informado.", response = Proposal.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposal/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public Proposal findById(@PathVariable("id") long id) {
		return proposalRepository.findById(id);
	}

	@ApiOperation(value = "Busca todas as propostas da base", response = Proposal.class, notes = "Esse metódo retorna todas as propostas cadastradas na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um lista de objetos proposal completos ", response = Proposal.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public List<Proposal> findAll() {
		return proposalRepository.findAll();
	}

	@ApiOperation(value = "Busca uma proposta pelo cpf", response = Proposal.class, notes = "Esse metódo recebe o cpf de uma proposta e retorna a proposta completa")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto proposal completo com o código de cpf informado.", response = Proposal.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/proposalByCpf/{cpf}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public Proposal findByCpf(String cpf) {
		return proposalRepository.findByCpf(cpf);
	}
}
