package com.calcard.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.calcard.api.model.Limit;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.Response;
import com.calcard.api.repository.LimitRepository;
import com.calcard.api.service.LimitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("v1/api")
@Service
@Api(tags="LIMITADORES" , description = "API para cadastro e busca dos limitadores para a lógica concessão de crédito")
public class LimitServiceImpl implements LimitService {

	@Autowired
	private LimitRepository limitRepository;

	@ApiOperation(value = "Cadastrar um novo limitador", response = Response.class, notes = "Esse metódo recebe os dados do limitador e salva na base de dados")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto response com código 1 caso o insert tenha sido feita com sucesso.", response = Response.class),
			@ApiResponse(code = 500, message = "Retorna um objeto response com código 0 e o erro ocassionado.", response = Response.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Response insert(@RequestBody Limit limit) {
		try {

			limitRepository.insert(limit);

			return new Response(1, "Limitador inserido com sucesso!");

		} catch (Exception e) {

			return new Response(0, e.getMessage());
		}
	}
	@ApiOperation(value = "Atualizar um limitador", response = Response.class, notes = "Esse metódo recebe os dados de um limitador e atualiza os dados do mesmo na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto response com código 1 caso a atualização tenha sido feita com sucesso.", response = Response.class),
			@ApiResponse(code = 500, message = "Retorna um objeto response com código 0 e o erro ocassionado.", response = Response.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limit", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Response update(@RequestBody Limit limit) {
		try {

			limitRepository.update(limit);

			return new Response(1, "Limitador atualizado com sucesso!");

		} catch (Exception e) {

			return new Response(0, e.getMessage());
		}
	}

	@ApiOperation(value = "Deletar um limitador", response = Response.class, notes = "Esse metódo recebe o id de um limitador e deleta o mesmo na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto response com código 1 caso o delete tenha sido feita com sucesso.", response = Response.class),
			@ApiResponse(code = 500, message = "Retorna um objeto response com código 0 e o erro ocassionado.", response = Response.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limit/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Response delete(@PathVariable("id") long id) {
		try {

			limitRepository.delete(id);

			return new Response(1, "Limitador deletado com sucesso!");

		} catch (Exception e) {

			return new Response(0, e.getMessage());
		}
	}

	@ApiOperation(value = "Busca um limitador pelo id", response = Proposal.class, notes = "Esse metódo recebe o id de um limitador e retorna o limitador completo")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto limit completo com o código de id informado.", response = Limit.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limit/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Limit findById(@PathVariable("id") long id) {
		return limitRepository.findById(id);
	}

	@ApiOperation(value = "Busca todos os limitadores da base", response = Limit.class, notes = "Esse metódo retorna todos os limitadores cadastradas na base")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um lista de objetos limit completos ", response = Limit.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody List<Limit> findAll() {
		return limitRepository.findAll();
	}
	
	@ApiOperation(value = "Busca um limitador qualificado para p valor informado", response = Limit.class, notes = "Esse metódo retorna o limitador adequado para o valor inserido")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna um objeto limit completo adequado ao valor passado", response = Limit.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/limitByValue/{value}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public @ResponseBody Limit findByValue(@PathVariable("value") double value) {
		return limitRepository.findByValue(value);
	}
	
}
