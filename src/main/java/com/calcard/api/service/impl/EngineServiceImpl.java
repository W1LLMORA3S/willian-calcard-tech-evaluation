package com.calcard.api.service.impl;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RulesEngineParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Limit;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.api.rules.approval.DivorcedApprovalRule;
import com.calcard.api.rules.approval.MarriedApprovalRule;
import com.calcard.api.rules.approval.SingleApprovalRule;
import com.calcard.api.rules.approval.WidowedApprovalRule;
import com.calcard.api.rules.creditPolicy.DivorcedPolicyRule;
import com.calcard.api.rules.creditPolicy.SinglePolicyRule;
import com.calcard.api.rules.creditPolicy.WidowedPolicyRule;
import com.calcard.api.rules.disapproval.DivorcedDisapprovalRule;
import com.calcard.api.rules.disapproval.MarriedDisapprovalRule;
import com.calcard.api.rules.disapproval.SingleDisapprovalRule;
import com.calcard.api.rules.disapproval.WidowedDisapprovalRule;
import com.calcard.api.service.EngineService;
import com.calcard.api.service.LimitService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("v1/api")
@Service
@Api(tags = "MOTOR DE REGRAS", description = "API para análise de aprovação ou reprovação das prospostas, além dos limites liberados")
public class EngineServiceImpl implements EngineService {

	@Autowired
	LimitService limitService;

	@ApiOperation(value = "Analizar a proposta e responder com a decisão e valores", response = AnalysisResult.class, notes = "Esse metódo recebe os dados da proposta e aplica uma série de regras definidas de modo a tomar a decisãl de aprovação ou negação da proposta e limites de crédito")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna a análise do resultado, indicando a aprovação, reprovação ou erro no processamento da proposta.", response = AnalysisResult.class),
			@ApiResponse(code = 500, message = "Em caso de erro, retorna o objeto resultado da análise com o status ERRO e a mensagem no campo message", response = AnalysisResult.class),
			@ApiResponse(code = 401, message = "A Api somente permitirá acesso aos seus recursos passando o Token gerado previamente")

	})
	@RequestMapping(value = "/analyze", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@Override
	public AnalysisResult analyzeProposal(Proposal proposal) {

		try {

			Facts facts = new Facts();

			AnalysisResult analysisResult = new AnalysisResult();
			facts.put("analysisResult", analysisResult);
			facts.put("proposal", proposal);

			Rules rules = new Rules();
			// Policy Rules
			rules.register(new DivorcedPolicyRule());
			rules.register(new SinglePolicyRule());
			rules.register(new WidowedPolicyRule());
			// Approval Rules
			rules.register(new DivorcedApprovalRule());
			rules.register(new MarriedApprovalRule());
			rules.register(new SingleApprovalRule());
			rules.register(new WidowedApprovalRule());
			// Disapproval Rules
			rules.register(new DivorcedDisapprovalRule());
			rules.register(new MarriedDisapprovalRule());
			rules.register(new SingleDisapprovalRule());
			rules.register(new WidowedDisapprovalRule());

			RulesEngineParameters parameters = new RulesEngineParameters().skipOnFirstAppliedRule(true)
					.skipOnFirstFailedRule(false).skipOnFirstNonTriggeredRule(false);

			RulesEngine rulesEngine = new DefaultRulesEngine(parameters);

			rulesEngine.fire(rules, facts);

			AnalysisResult analysisResultFired = facts.get("analysisResult");
			Proposal proposalFired = facts.get("proposal");

			if (analysisResultFired.getStatusProposal() != null) {

				if (analysisResultFired.getStatusProposal()
						.equalsIgnoreCase(StatusProposal.APPROVED.getStatusProposal())) {
					Limit limit = limitService.findByValue(proposalFired.getIncomeReal());
					analysisResultFired.setLimitMin(limit.getMin());
					analysisResultFired.setLimitMax(limit.getMax());
					analysisResultFired.setMessage(limit.getDescription());
				}
				return analysisResultFired;
			} else {
				throw new java.lang.Error("Não foi possível executar as regras para a proposta");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;
	}

}
