package com.calcard.api.service;

import java.util.List;

import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.Response;

public interface ProposalService {
	AnalysisResult insert(Proposal proposal);
	Response update(Proposal proposal);
	Response delete(long id);
	Proposal findById(long id);
	Proposal findByCpf(String cpf);
	List<Proposal> findAll();
}
