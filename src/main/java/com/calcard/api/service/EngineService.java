package com.calcard.api.service;

import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;

public interface EngineService {
	AnalysisResult analyzeProposal(Proposal proposal) throws Error;
}
