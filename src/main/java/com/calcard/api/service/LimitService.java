package com.calcard.api.service;

import java.util.List;

import com.calcard.api.model.Limit;
import com.calcard.api.model.Response;

public interface LimitService {
	Response insert(Limit limit);

	Response update(Limit limit);

	Response delete(long id);

	Limit findById(long id);
	
	Limit findByValue(double value);

	List<Limit> findAll();
}
