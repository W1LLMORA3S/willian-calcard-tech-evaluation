package com.calcard.api.service;

import java.util.List;

import com.calcard.api.model.AnalysisResult;

public interface AnalysisResultService {
	AnalysisResult findByIdProposal(long id);
	AnalysisResult findByCPF(String CPF);
	List<AnalysisResult> findAll();
}
