package com.calcard.api.rules.disapproval;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.MaritalStatus;
import com.calcard.api.model.enumeration.Messages;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.config.constants.RulesConstants;

@Rule(name = "married approval rule", description = "definition of rule for approval proposal for marrieds", priority = 2)
public class DivorcedDisapprovalRule {
	@Condition
	public boolean isAproved(@Fact("proposal") Proposal proposal) {
		double incomeReal = (proposal.getIncome() / (proposal.getDependents() + 1))
				* RulesConstants.DIVORCED_REDUCTION_RATE;
		proposal.setIncomeReal(incomeReal);

		return incomeReal < RulesConstants.MIN_APPROVAL && proposal.getMaritalStatus()
				.equalsIgnoreCase(MaritalStatus.DIVORCED.getMaritalStatus());
	}

	@Action
	public void setStatus(@Fact("analysisResult") AnalysisResult analysisResult) {
		analysisResult.setStatusProposal(StatusProposal.DISAPPROVED.getStatusProposal());
		analysisResult.setMessage(Messages.LOW_INCOME.getDisapprovedMessage());
	}

}