package com.calcard.api.rules.creditPolicy;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.MaritalStatus;
import com.calcard.api.model.enumeration.Messages;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.config.constants.RulesConstants;

@Rule(name = "single policy credit rule", description = "definition of rule for single policy credit", priority = 1)
public class SinglePolicyRule {
	@Condition
	public boolean isBlocked(@Fact("proposal") Proposal proposal)  {

		return proposal.getMaritalStatus().equals(MaritalStatus.SINGLE.getMaritalStatus())
				&& proposal.getDependents() > RulesConstants.MAX_DEPENDENTS_SINGLE;
	}

	@Action
	public void setStatus(@Fact("analysisResult") AnalysisResult analysisResult) {
		analysisResult.setStatusProposal(StatusProposal.DISAPPROVED.getStatusProposal());
		analysisResult.setMessage(Messages.CREDIT_POLICY.getDisapprovedMessage());
	}

}