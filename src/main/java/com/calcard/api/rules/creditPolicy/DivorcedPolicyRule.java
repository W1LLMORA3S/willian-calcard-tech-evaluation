package com.calcard.api.rules.creditPolicy;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.MaritalStatus;
import com.calcard.api.model.enumeration.Messages;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.config.constants.RulesConstants;

@Rule(name = "divorced policy credit rule", description = "definition of rule for divorced policy credit", priority = 1)
public class DivorcedPolicyRule {
	@Condition
	public boolean isBlocked(@Fact("proposal") Proposal proposal) {

		return proposal.getMaritalStatus().equals(MaritalStatus.DIVORCED.getMaritalStatus())
				&& proposal.getDependents() > RulesConstants.MAX_DEPENDENTS_DIVORCED;
	}

	@Action
	public void setStatus(@Fact("analysisResult") AnalysisResult analysisResult) {
		analysisResult.setStatusProposal(StatusProposal.DISAPPROVED.getStatusProposal());
		analysisResult.setMessage(Messages.CREDIT_POLICY.getDisapprovedMessage());
	}

}