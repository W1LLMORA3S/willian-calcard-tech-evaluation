package com.calcard.api.rules.creditPolicy;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.MaritalStatus;
import com.calcard.api.model.enumeration.Messages;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.config.constants.RulesConstants;

@Rule(name = "widowed policy credit rule", description = "definition of rule for widowed policy credit", priority = 1)
public class WidowedPolicyRule {
	@Condition
	public boolean isBlocked(@Fact("proposal") Proposal proposal) {

		return proposal.getMaritalStatus().equals(MaritalStatus.WIDOWED.getMaritalStatus())
				&& proposal.getDependents() > RulesConstants.MAX_DEPENDENTS_WIDOWED;
	}

	@Action
	public void setStatus(@Fact("analysisResult") AnalysisResult analysisResult) {
		analysisResult.setStatusProposal(StatusProposal.DISAPPROVED.getStatusProposal());
		analysisResult.setMessage(Messages.CREDIT_POLICY.getDisapprovedMessage());
	}

}