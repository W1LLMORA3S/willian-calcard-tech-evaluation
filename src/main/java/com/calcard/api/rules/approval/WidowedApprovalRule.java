package com.calcard.api.rules.approval;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import com.calcard.api.model.AnalysisResult;
import com.calcard.api.model.Proposal;
import com.calcard.api.model.enumeration.MaritalStatus;
import com.calcard.api.model.enumeration.StatusProposal;
import com.calcard.config.constants.RulesConstants;

@Rule(name = "single approval rule", description = "definition of rule for approval proposal for singles", priority = 2)
public class WidowedApprovalRule {
	@Condition
	public boolean isAproved(@Fact("proposal") Proposal proposal) {
		double incomeReal = (proposal.getIncome()
				/ (proposal.getDependents() + 1)) * RulesConstants.WIDOWED_REDUCTION_RATE;
		proposal.setIncomeReal(incomeReal);

		return incomeReal > RulesConstants.MIN_APPROVAL && proposal.getMaritalStatus()
				.equalsIgnoreCase(MaritalStatus.WIDOWED.getMaritalStatus());
	}

	@Action
	public void setStatus(@Fact("analysisResult") AnalysisResult analysisResult) {
		analysisResult.setStatusProposal(StatusProposal.APPROVED.getStatusProposal());
	}
}