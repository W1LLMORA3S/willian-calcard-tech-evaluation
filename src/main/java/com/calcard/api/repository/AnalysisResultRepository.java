package com.calcard.api.repository;

import java.util.List;

import com.calcard.api.model.AnalysisResult;
public interface AnalysisResultRepository {
	
	AnalysisResult findResultByIdProposal(long id);
	
	AnalysisResult findResultByCpf(String cpf);
	
	AnalysisResult findById(long id);

	List<AnalysisResult> findAll();

	int insert(AnalysisResult analysisResult);

	void delete(long id);

}
