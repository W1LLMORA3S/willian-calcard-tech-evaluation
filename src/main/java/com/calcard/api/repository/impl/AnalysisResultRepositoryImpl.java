package com.calcard.api.repository.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calcard.api.model.AnalysisResult;
import com.calcard.api.repository.AnalysisResultRepository;

@Repository
@Transactional
public class AnalysisResultRepositoryImpl implements AnalysisResultRepository {
	private static final String ANALYSIS_RESULT_MAPPER_NAMESPACE = "com.calcard.mapper.AnalysisResultMapper";

	@Autowired
	private SqlSession sqlSession;

	@Override
	public AnalysisResult findResultByIdProposal(long id) {
		return this.sqlSession.selectOne(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".findResultByIdProposal", id);

	}

	@Override
	public AnalysisResult findById(long id) {
		return this.sqlSession.selectOne(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".findById", id);
	}

	@Override
	public List<AnalysisResult> findAll() {
		return this.sqlSession.selectList(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".findAll");
	}

	@Override
	public int insert(AnalysisResult analysisResult) {
		return this.sqlSession.insert(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".insert", analysisResult);

	}

	@Override
	public void delete(long id) {
		this.sqlSession.delete(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".delete", id);
	}

	@Override
	public AnalysisResult findResultByCpf(String cpf) {
		return this.sqlSession.selectOne(ANALYSIS_RESULT_MAPPER_NAMESPACE + ".findResultByCpf", cpf);
	}
}
