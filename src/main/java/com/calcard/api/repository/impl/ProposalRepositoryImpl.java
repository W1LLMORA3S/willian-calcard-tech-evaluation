package com.calcard.api.repository.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calcard.api.model.Proposal;
import com.calcard.api.repository.ProposalRepository;

@Repository
@Transactional
public class ProposalRepositoryImpl implements ProposalRepository {

	private static final String PROPOSAL_MAPPER_NAMESPACE = "com.calcard.api.mapper.ProposalMapper";

	@Autowired
	private SqlSession sqlSession;

	@Override
	public Proposal findById(long id) {
		return this.sqlSession.selectOne(PROPOSAL_MAPPER_NAMESPACE + ".findById", id);
	}

	@Override
	public List<Proposal> findAll() {
		return this.sqlSession.selectList(PROPOSAL_MAPPER_NAMESPACE + ".findAll");
	}

	@Override
	public void update(Proposal proposal) {
		this.sqlSession.update(PROPOSAL_MAPPER_NAMESPACE + ".update", proposal);
	}

	@Override
	public int insert(Proposal proposal) {
		return this.sqlSession.insert(PROPOSAL_MAPPER_NAMESPACE + ".insert", proposal);
	}

	@Override
	public void delete(long id) {
		this.sqlSession.delete(PROPOSAL_MAPPER_NAMESPACE + ".delete", id);
		
	}

	@Override
	public Proposal findByCpf(String cpf) {
		return this.sqlSession.selectOne(PROPOSAL_MAPPER_NAMESPACE + ".findByCpf", cpf);
	}

}
