package com.calcard.api.repository.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calcard.api.model.Limit;
import com.calcard.api.repository.LimitRepository;

@Repository
@Transactional
public class LimitRepositoryImpl implements LimitRepository {

	private static final String LIMIT_MAPPER_NAMESPACE = "com.calcard.api.mapper.LimitMapper";

	@Autowired
	private SqlSession sqlSession;

	@Override
	public Limit findById(long id) {
		return this.sqlSession.selectOne(LIMIT_MAPPER_NAMESPACE + ".findById", id);
	}

	@Override
	public List<Limit> findAll() {
		return this.sqlSession.selectList(LIMIT_MAPPER_NAMESPACE + ".findAll");
	}

	@Override
	public void update(Limit limit) {
		this.sqlSession.update(LIMIT_MAPPER_NAMESPACE + ".update", limit);
	}

	@Override
	public int insert(Limit limit) {
		return this.sqlSession.insert(LIMIT_MAPPER_NAMESPACE + ".insert", limit);
	}

	@Override
	public void delete(long id) {
		this.sqlSession.delete(LIMIT_MAPPER_NAMESPACE + ".delete", id);

	}

	@Override
	public Limit findByValue(double value) {
		return this.sqlSession.selectOne(LIMIT_MAPPER_NAMESPACE + ".findByValue", value);
	}

}
