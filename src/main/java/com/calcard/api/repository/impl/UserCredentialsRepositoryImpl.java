package com.calcard.api.repository.impl;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calcard.api.model.UserCredentials;
import com.calcard.api.repository.UserCredentialsRepository;

@Repository
@Transactional
public class UserCredentialsRepositoryImpl implements UserCredentialsRepository {

	private static final String USER_MAPPER_NAMESPACE = "com.calcard.api.mapper.UserMapper";

	@Autowired
	private SqlSession sqlSession;

	@Override
	public UserCredentials findByUsernameAndPassword(String username, String password) {
		HashMap<String, Object> user = new HashMap<>();
		user.put("username", username);
		user.put("password", password);
		return this.sqlSession.selectOne(USER_MAPPER_NAMESPACE + ".findByUsernameAndPassword", user);
	}

}
