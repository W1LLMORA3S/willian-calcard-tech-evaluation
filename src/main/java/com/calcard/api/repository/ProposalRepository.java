package com.calcard.api.repository;

import java.util.List;

import com.calcard.api.model.Proposal;

public interface ProposalRepository {
	
	Proposal findById(long id);
	
	Proposal findByCpf(String cpf);

	List<Proposal> findAll();

	void update(Proposal proposal);

	int insert(Proposal proposal);

	void delete(long id);
}
