package com.calcard.api.repository;

import com.calcard.api.model.UserCredentials;

public interface UserCredentialsRepository {
	UserCredentials findByUsernameAndPassword(String username, String password);
}
