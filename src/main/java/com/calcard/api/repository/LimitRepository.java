package com.calcard.api.repository;

import java.util.List;

import com.calcard.api.model.Limit;

public interface LimitRepository {

	Limit findById(long id);
	
	Limit findByValue(double value);

	List<Limit> findAll();

	void update(Limit limit);

	int insert(Limit limit);

	void delete(long id);
}
